///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Kendal Oya <kendalo@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @27_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <stdlib.h>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

   Animal::Animal(){
      cout << "." << endl;
   }

   Animal::~Animal(){
      cout << "X" << endl;
   }


void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}
//color
const Color Animal::getRandomColor(){
   int color = rand() % 6;
   switch(color){
      case 0:        return BLACK;              break;
      case 1:        return WHITE;              break;
      case 2:        return RED;                break;
      case 3:        return SILVER;             break;
      case 4:        return YELLOW;             break;
      case 5:        return BROWN;              break;
   }
   return WHITE;
}
//bool
const bool Animal::getRandomBool(){
   int y = rand() % 2;

   switch(y){
      case 0:
         return false;
         break;
      case 1:
         return true;
         break;
   }
   return true;
}
//gender
const enum Gender Animal::getRandomGender(){
   int gender = rand() % 2;

   switch(gender){
      case 0:       
         return MALE;               
         break;
      case 1:        
         return FEMALE;             
         break;
   }
   return MALE;
}
//weight
const float Animal:: getRandomWeight(const float from, const float to){
   int span = (int) (to - from);
   float randomWeight = from + (rand() % span);
   return randomWeight;
}
//name
const string Animal::getRandomName(){
   int length = (rand() % 6) + 4;
   char random[length];
//capital
    random[0] = (char) (65 + rand() % 26);
//lowercase
   for (int i=1;i<length;i++){
       random[i] = (char) (97 + rand() % 26);
   }
   return random;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 
   switch (color){
      case BLACK: return string("Black"); break;
      case WHITE: return string("White"); break;
      case RED: return string("Red"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN: return string("Brown"); break;
   }
   return string("Unknown");
};
	
} // namespace animalfarm
