///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
///
/// @author Kendal Oya <kendalo@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   29 Mar 2021
//////////////////////////////////////////////////////////////////////////////

#pragma once
#include "node.hpp"

namespace animalfarm{

   class SingleLinkedList{
      protected:
         Node* head = nullptr;
      public:
         const bool empty() const;
         void push_front( Node* newNode );
         Node* pop_front();
         Node* get_first() const;
         Node* get_next( const Node* currentNode ) const;
         unsigned int size() const;
   };
}
