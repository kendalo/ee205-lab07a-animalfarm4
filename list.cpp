///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
///
/// @author Kendal Oya <kendalo@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   29 Mar 2021
//////////////////////////////////////////////////////////////////////////////
//

#include <iostream>
#include <cstdlib>
#include "list.hpp"
#include "node.hpp"
namespace animalfarm{

   Node* head = new Node();
   const bool SingleLinkedList::empty() const{
      return head == nullptr;
   }

   void SingleLinkedList::push_front( Node* newNode ){
      newNode->next = head;
      head = newNode;
   }

   Node* SingleLinkedList::pop_front(){
      
         Node* temp = head;
         head = head->next;
         return temp;
      
   }

   Node* SingleLinkedList::get_first() const{
      return head;
   }

   Node* SingleLinkedList::get_next( const Node* currentNode) const{
      return currentNode->next;
   }

   unsigned int SingleLinkedList::size() const{
      unsigned int i;
      Node* temp = head;
      for(i=0; temp != nullptr; i++){
         temp = temp->next;
      }
      return i;
   }
}
